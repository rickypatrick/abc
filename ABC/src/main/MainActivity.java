package com.eeculink.abc;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import java.util.HashMap;


public class MainActivity extends ActionBarActivity {
    

    SoundManager snd;
    int A1, A2, A3, B1, B2, B3, C1, C2, C3, D1, D2, D3, E1, E2, E3, F1, F2, F3, G1, G2, G3, H1, H2, H3,
    I1, I2, I3, J1, J2, J3, K1, K2, K3, L1, L2, L3, M1, M2, M3, N1, N2, N3, O1, O2, O3, P1, P2, P3, Q1,
    Q2, Q3, R1, R2, R3, S1, S2, S3, T1, T2, T3, U1, U2, U3, V1, V2, V3, W1, W2, W3, X1, X2, X3, Y1, Y2, Y3, Z1, Z2, Z3;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        snd = new SoundManager(getApplicationContext());
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        A1 = snd.load(R.raw.abc_a);
        A2 = snd.load(R.raw.abc_a2);
        A3 = snd.load(R.raw.abc_a3);
        B1 = snd.load(R.raw.abc_b);
        B2 = snd.load(R.raw.abc_b2);
        B3 = snd.load(R.raw.abc_b3);
        C1 = snd.load(R.raw.abc_c);
        C2 = snd.load(R.raw.abc_c2);
        C3 = snd.load(R.raw.abc_c3);
        D1 = snd.load(R.raw.abc_d);
        D2 = snd.load(R.raw.abc_d2);
        D3 = snd.load(R.raw.abc_d3);
        E1 = snd.load(R.raw.abc_e);
        E2 = snd.load(R.raw.abc_e2);
        E3 = snd.load(R.raw.abc_e3);
        F1 = snd.load(R.raw.abc_f);
        F2 = snd.load(R.raw.abc_f2);
        F3 = snd.load(R.raw.abc_f3);
        G1 = snd.load(R.raw.abc_g);
        G2 = snd.load(R.raw.abc_g2);
        G3 = snd.load(R.raw.abc_g3);
        H1 = snd.load(R.raw.abc_h);
        H2 = snd.load(R.raw.abc_h2);
        H3 = snd.load(R.raw.abc_h3);
        I1 = snd.load(R.raw.abc_i);
        I2 = snd.load(R.raw.abc_i2);
        I3 = snd.load(R.raw.abc_i3);
        J1 = snd.load(R.raw.abc_j);
        J2 = snd.load(R.raw.abc_j2);
        J3 = snd.load(R.raw.abc_j3);
        K1 = snd.load(R.raw.abc_k);
        K2 = snd.load(R.raw.abc_k2);
        K3 = snd.load(R.raw.abc_k3);
        L1 = snd.load(R.raw.abc_l);
        L2 = snd.load(R.raw.abc_l2);
        L3 = snd.load(R.raw.abc_l3);
        M1 = snd.load(R.raw.abc_m);
        M2 = snd.load(R.raw.abc_m2);
        M3 = snd.load(R.raw.abc_m3);
        N1 = snd.load(R.raw.abc_n);
        N2 = snd.load(R.raw.abc_n2);
        N3 = snd.load(R.raw.abc_n3);
        O1 = snd.load(R.raw.abc_o);
        O2 = snd.load(R.raw.abc_o2);
        O3 = snd.load(R.raw.abc_o3);
        P1 = snd.load(R.raw.abc_p);
        P2 = snd.load(R.raw.abc_p2);
        P3 = snd.load(R.raw.abc_p3);
        Q1 = snd.load(R.raw.abc_q);
        Q2 = snd.load(R.raw.abc_q2);
        Q3 = snd.load(R.raw.abc_q3);
        R1 = snd.load(R.raw.abc_r);
        R2 = snd.load(R.raw.abc_r2);
        R3 = snd.load(R.raw.abc_r3);
        S1 = snd.load(R.raw.abc_s);
        S2 = snd.load(R.raw.abc_s2);
        S3 = snd.load(R.raw.abc_s3);
        T1 = snd.load(R.raw.abc_t);
        T2 = snd.load(R.raw.abc_t2);
        T3 = snd.load(R.raw.abc_t3);
        U1 = snd.load(R.raw.abc_u);
        U2 = snd.load(R.raw.abc_u2);
        U3 = snd.load(R.raw.abc_u3);
        V1 = snd.load(R.raw.abc_v);
        V2 = snd.load(R.raw.abc_v2);
        V3 = snd.load(R.raw.abc_v3);
        W1 = snd.load(R.raw.abc_w);
        W2 = snd.load(R.raw.abc_w2);
        W3 = snd.load(R.raw.abc_w3);
        X1 = snd.load(R.raw.abc_x);
        X2 = snd.load(R.raw.abc_x2);
        X3 = snd.load(R.raw.abc_x3);
        Y1 = snd.load(R.raw.abc_y);
        Y2 = snd.load(R.raw.abc_y2);
        Y3 = snd.load(R.raw.abc_y3);
        Z1 = snd.load(R.raw.abc_z);
        Z2 = snd.load(R.raw.abc_z2);
        Z3 = snd.load(R.raw.abc_z3);


        final MediaPlayer aPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_a);
        final MediaPlayer aPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_a2);
        final MediaPlayer aPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_a3);

        final MediaPlayer bPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_b);
        final MediaPlayer bPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_b2);
        final MediaPlayer bPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_b3);

        final MediaPlayer cPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_c);
        final MediaPlayer cPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_c2);
        final MediaPlayer cPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_c3);

        final MediaPlayer dPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_d);
        final MediaPlayer dPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_d2);
        final MediaPlayer dPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_d3);

        final MediaPlayer ePlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_e);
        final MediaPlayer ePlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_e2);
        final MediaPlayer ePlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_e3);

        final MediaPlayer fPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_f);
        final MediaPlayer fPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_f2);
        final MediaPlayer fPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_f3);

        final MediaPlayer gPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_g);
        final MediaPlayer gPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_g2);
        final MediaPlayer gPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_g3);

        final MediaPlayer hPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_h);
        final MediaPlayer hPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_h2);
        final MediaPlayer hPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_h3);

        final MediaPlayer iPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_i);
        final MediaPlayer iPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_i2);
        final MediaPlayer iPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_i3);

        final MediaPlayer jPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_j);
        final MediaPlayer jPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_j2);
        final MediaPlayer jPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_j3);

        final MediaPlayer kPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_k);
        final MediaPlayer kPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_k2);
        final MediaPlayer kPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_k3);

        final MediaPlayer lPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_l);
        final MediaPlayer lPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_l2);
        final MediaPlayer lPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_l3);

        final MediaPlayer mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_m);
        final MediaPlayer mPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_m2);
        final MediaPlayer mPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_m3);

        final MediaPlayer nPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_n);
        final MediaPlayer nPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_n2);
        final MediaPlayer nPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_n3);

        final MediaPlayer oPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_o);
        final MediaPlayer oPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_o2);
        final MediaPlayer oPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_o3);

        final MediaPlayer pPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_p);
        final MediaPlayer pPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_p2);
        final MediaPlayer pPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_p3);

        final MediaPlayer qPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_q);
        final MediaPlayer qPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_q2);
        final MediaPlayer qPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_q3);

        final MediaPlayer rPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_r);
        final MediaPlayer rPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_r2);
        final MediaPlayer rPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_r3);

        final MediaPlayer sPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_s);
        final MediaPlayer sPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_s2);
        final MediaPlayer sPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_s3);

        final MediaPlayer tPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_t);
        final MediaPlayer tPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_t2);
        final MediaPlayer tPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_t3);

        final MediaPlayer uPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_u);
        final MediaPlayer uPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_u2);
        final MediaPlayer uPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_u3);

        final MediaPlayer vPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_v);
        final MediaPlayer vPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_v2);
        final MediaPlayer vPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_v3);

        final MediaPlayer wPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_w);
        final MediaPlayer wPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_w2);
        final MediaPlayer wPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_w3);

        final MediaPlayer xPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_x);
        final MediaPlayer xPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_x2);
        final MediaPlayer xPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_x3);

        final MediaPlayer yPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_y);
        final MediaPlayer yPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_y2);
        final MediaPlayer yPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_y3);

        final MediaPlayer zPlayer = MediaPlayer.create(getApplicationContext(), R.raw.abc_z);
        final MediaPlayer zPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.abc_z2);
        final MediaPlayer zPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.abc_z3);



        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.container, new PlaceholderFragment())
                    .commit();


        }




        final ImageButton aButton = (ImageButton) findViewById(R.id.aButton);

        aButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.aButton:
                        Toast.makeText(getApplicationContext(), "a button clicked", 4).show();

                        snd.play(A1);
 //                       aPlayer.start();
                        aButton.setId(102);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        aButton.startAnimation(wobble);
                        break;
                    case 101:
                        Toast.makeText(getApplicationContext(), "a button clicked", 4).show();
                        snd.play(A1);
                      //  aPlayer.start();
                        aButton.setId(102);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        aButton.startAnimation(wobble2);
                        break;
                    case 102:
                        Toast.makeText(getApplicationContext(), "a button clicked TWICE", 4).show();
                        snd.play(A2);
                       // aPlayer2.start();
                        aButton.setId(103);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        aButton.startAnimation(wobble3);
                        aButton.setImageResource(R.drawable.apple);
                        break;
                    case 103:
                        Toast.makeText(getApplicationContext(), "a button clicked THRICE!!", 4).show();
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        aButton.startAnimation(fadein);
                        snd.play(A3);
                       // aPlayer3.start();
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        aButton.setId(101);
                        aButton.setImageResource(R.drawable.a);
                        break;

                    default:
                        break;

                }


            }


        });

        final ImageButton bButton = (ImageButton) findViewById(R.id.bButton);

        bButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.bButton:
                        Toast.makeText(getApplicationContext(), "B button clicked", 4).show();
                        snd.play(B1);
                        //bPlayer.start();
                        bButton.setId(202);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        bButton.startAnimation(wobble);
                        break;
                    case 201:
                        Toast.makeText(getApplicationContext(), "B button clicked", 4).show();
                        snd.play(B1);
                       // bPlayer.start();

                        bButton.setId(202);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        bButton.startAnimation(wobble2);
                        break;
                    case 202:
                        Toast.makeText(getApplicationContext(), "B button clicked TWICE", 4).show();
                        snd.play(B2);
                     //   bPlayer2.start();
                        bButton.setId(203);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        bButton.startAnimation(wobble3);
                        bButton.setImageResource(R.drawable.banana);
                        break;
                    case 203:
                        Toast.makeText(getApplicationContext(), "B button clicked THRICE!!", 4).show();
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        bButton.startAnimation(fadein);
                        snd.play(B3);
                    //    bPlayer3.start();



                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        bButton.setId(201);
                        bButton.setImageResource(R.drawable.b);
                        break;

                    default:
                        break;

                }


            }


        });


        final ImageButton cButton = (ImageButton) findViewById(R.id.cButton);

        cButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.cButton:
                        snd.play(C1);
                        cButton.setId(302);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        cButton.startAnimation(wobble);
                        break;
                    case 301:
                        snd.play(C1);
                        cButton.setId(302);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        cButton.startAnimation(wobble2);
                        break;
                    case 302:
                        snd.play(C2);
                        cButton.setId(303);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        cButton.startAnimation(wobble3);
                        cButton.setImageResource(R.drawable.cookie);
                        break;
                    case 303:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        cButton.startAnimation(fadein);
                        snd.play(C3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        cButton.setId(301);
                        cButton.setImageResource(R.drawable.c);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton dButton = (ImageButton) findViewById(R.id.dButton);

        dButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.dButton:
                        snd.play(D1);
                        dButton.setId(402);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        dButton.startAnimation(wobble);
                        break;
                    case 401:
                        snd.play(D1);
                        dButton.setId(402);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        dButton.startAnimation(wobble2);
                        break;
                    case 402:
                        snd.play(D2);
                        dButton.setId(403);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        dButton.startAnimation(wobble3);
                        dButton.setImageResource(R.drawable.donut);
                        break;
                    case 403:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        dButton.startAnimation(fadein);
                        snd.play(D3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        dButton.setId(401);
                        dButton.setImageResource(R.drawable.d);
                        break;

                    default:
                        break;

                }


            }


        });

        final ImageButton eButton = (ImageButton) findViewById(R.id.eButton);
        eButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.eButton:
                        snd.play(E1);
                        eButton.setId(502);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        eButton.startAnimation(wobble);
                        break;
                    case 501:
                        snd.play(E1);
                        eButton.setId(502);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        eButton.startAnimation(wobble2);
                        break;
                    case 502:
                        snd.play(E2);
                        eButton.setId(503);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        eButton.startAnimation(wobble3);
                        eButton.setImageResource(R.drawable.egg);
                        break;
                    case 503:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        eButton.startAnimation(fadein);
                        snd.play(E3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        eButton.setId(501);
                        eButton.setImageResource(R.drawable.e);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton fButton = (ImageButton) findViewById(R.id.fButton);
        fButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.fButton:
                        snd.play(F1);
                        fButton.setId(602);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        fButton.startAnimation(wobble);
                        break;
                    case 601:
                        snd.play(F1);
                        fButton.setId(602);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        fButton.startAnimation(wobble2);
                        break;
                    case 602:
                        snd.play(F2);
                        fButton.setId(603);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        fButton.startAnimation(wobble3);
                        fButton.setImageResource(R.drawable.fish);
                        break;
                    case 603:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        fButton.startAnimation(fadein);
                        snd.play(F3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        fButton.setId(601);
                        fButton.setImageResource(R.drawable.f);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton gButton = (ImageButton) findViewById(R.id.gButton);
        gButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.gButton:
                        snd.play(G1);
                        gButton.setId(702);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        gButton.startAnimation(wobble);
                        break;
                    case 701:
                        snd.play(G1);
                        gButton.setId(702);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        gButton.startAnimation(wobble2);
                        break;
                    case 702:
                        snd.play(G2);
                        gButton.setId(703);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        gButton.startAnimation(wobble3);
                        gButton.setImageResource(R.drawable.grapes);
                        break;
                    case 703:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        gButton.startAnimation(fadein);
                        snd.play(G3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        gButton.setId(701);
                        gButton.setImageResource(R.drawable.g);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton hButton = (ImageButton) findViewById(R.id.hButton);
        hButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.hButton:
                        snd.play(H1);
                        hButton.setId(802);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        hButton.startAnimation(wobble);
                        break;
                    case 801:
                        snd.play(H1);
                        hButton.setId(802);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        hButton.startAnimation(wobble2);
                        break;
                    case 802:
                        snd.play(H2);
                        hButton.setId(803);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        hButton.startAnimation(wobble3);
                        hButton.setImageResource(R.drawable.hamburger);
                        break;
                    case 803:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        hButton.startAnimation(fadein);
                        snd.play(H3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1500); }catch(InterruptedException e){ }
                        hButton.setId(801);
                        hButton.setImageResource(R.drawable.h);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton iButton = (ImageButton) findViewById(R.id.iButton);
        iButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.iButton:
                        snd.play(I1);
                        iButton.setId(902);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        iButton.startAnimation(wobble);
                        break;
                    case 901:
                        snd.play(I1);
                        iButton.setId(902);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        iButton.startAnimation(wobble2);
                        break;
                    case 902:
                        snd.play(I2);
                        iButton.setId(903);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        iButton.startAnimation(wobble3);
                        iButton.setImageResource(R.drawable.icecream);
                        break;
                    case 903:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        iButton.startAnimation(fadein);
                        snd.play(I3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        iButton.setId(901);
                        iButton.setImageResource(R.drawable.i);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton jButton = (ImageButton) findViewById(R.id.jButton);
        jButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.jButton:
                        snd.play(J1);
                        jButton.setId(112);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        jButton.startAnimation(wobble);
                        break;
                    case 111:
                        snd.play(J1);
                        jButton.setId(112);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        jButton.startAnimation(wobble2);
                        break;
                    case 112:
                        snd.play(J2);
                        jButton.setId(113);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        jButton.startAnimation(wobble3);
                        jButton.setImageResource(R.drawable.jelly);
                        break;
                    case 113:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        jButton.startAnimation(fadein);
                        snd.play(J3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        jButton.setId(111);
                        jButton.setImageResource(R.drawable.j);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton kButton = (ImageButton) findViewById(R.id.kButton);
        kButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.kButton:
                        snd.play(K1);
                        kButton.setId(122);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        kButton.startAnimation(wobble);
                        break;
                    case 121:
                        snd.play(K1);
                        kButton.setId(122);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        kButton.startAnimation(wobble2);
                        break;
                    case 122:
                        snd.play(K2);
                        kButton.setId(123);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        kButton.startAnimation(wobble3);
                        kButton.setImageResource(R.drawable.kiwi);
                        break;
                    case 123:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        kButton.startAnimation(fadein);
                        snd.play(K3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        kButton.setId(121);
                        kButton.setImageResource(R.drawable.k);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton lButton = (ImageButton) findViewById(R.id.lButton);
        lButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.lButton:
                        snd.play(L1);
                        lButton.setId(132);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        lButton.startAnimation(wobble);
                        break;
                    case 131:
                        snd.play(L1);
                        lButton.setId(132);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        lButton.startAnimation(wobble2);
                        break;
                    case 132:
                        snd.play(L2);
                        lButton.setId(133);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        lButton.startAnimation(wobble3);
                        lButton.setImageResource(R.drawable.lollipop);
                        break;
                    case 133:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        lButton.startAnimation(fadein);
                        snd.play(L3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        lButton.setId(131);
                        lButton.setImageResource(R.drawable.l);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton mButton = (ImageButton) findViewById(R.id.mButton);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.mButton:
                        snd.play(M1);

                        mButton.setId(142);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        mButton.startAnimation(wobble);
                        break;
                    case 141:
                        snd.play(M1);
                        mButton.setId(142);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        mButton.startAnimation(wobble2);
                        break;
                    case 142:
                        snd.play(M2);
                        mButton.setId(143);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        mButton.startAnimation(wobble3);
                        mButton.setImageResource(R.drawable.macaroni);
                        break;
                    case 143:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        mButton.startAnimation(fadein);
                        snd.play(M3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        mButton.setId(141);
                        mButton.setImageResource(R.drawable.m);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton nButton = (ImageButton) findViewById(R.id.nButton);
        nButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.nButton:
                        snd.play(N1);
                        nButton.setId(152);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        nButton.startAnimation(wobble);
                        break;
                    case 151:
                        snd.play(N1);
                        nButton.setId(152);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        nButton.startAnimation(wobble2);
                        break;
                    case 152:
                        snd.play(N2);
                        nButton.setId(153);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        nButton.startAnimation(wobble3);
                        nButton.setImageResource(R.drawable.nuts);
                        break;
                    case 153:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        nButton.startAnimation(fadein);
                        snd.play(N3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        nButton.setId(151);
                        nButton.setImageResource(R.drawable.n);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton oButton = (ImageButton) findViewById(R.id.oButton);
        oButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.oButton:
                        snd.play(O1);
                        oButton.setId(162);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        oButton.startAnimation(wobble);
                        break;
                    case 161:
                        snd.play(O1);
                        oButton.setId(162);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        oButton.startAnimation(wobble2);
                        break;
                    case 162:
                        snd.play(O2);
                        oButton.setId(163);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        oButton.startAnimation(wobble3);
                        oButton.setImageResource(R.drawable.orange);
                        break;
                    case 163:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        oButton.startAnimation(fadein);
                        snd.play(O3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        oButton.setId(161);
                        oButton.setImageResource(R.drawable.o);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton pButton = (ImageButton) findViewById(R.id.pButton);
        pButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.pButton:
                        snd.play(P1);
                        pButton.setId(172);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        pButton.startAnimation(wobble);
                        break;
                    case 171:
                        snd.play(P1);
                        pButton.setId(172);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        pButton.startAnimation(wobble2);
                        break;
                    case 172:
                        snd.play(P2);
                        pButton.setId(173);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        pButton.startAnimation(wobble3);
                        pButton.setImageResource(R.drawable.pizza);
                        break;
                    case 173:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        pButton.startAnimation(fadein);
                        snd.play(P3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        pButton.setId(171);
                        pButton.setImageResource(R.drawable.p);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton qButton = (ImageButton) findViewById(R.id.qButton);
        qButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.qButton:
                        snd.play(Q1);
                        qButton.setId(182);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        qButton.startAnimation(wobble);
                        break;
                    case 181:
                        snd.play(Q1);
                        qButton.setId(182);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        qButton.startAnimation(wobble2);
                        break;
                    case 182:
                        snd.play(Q2);
                        qButton.setId(183);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        qButton.startAnimation(wobble3);
                        qButton.setImageResource(R.drawable.queso);
                        break;
                    case 183:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        qButton.startAnimation(fadein);
                        snd.play(Q3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        qButton.setId(181);
                        qButton.setImageResource(R.drawable.q);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton rButton = (ImageButton) findViewById(R.id.rButton);
        rButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.rButton:
                        snd.play(R1);
                        rButton.setId(192);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        rButton.startAnimation(wobble);
                        break;
                    case 191:
                        snd.play(R1);
                        rButton.setId(192);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        rButton.startAnimation(wobble2);
                        break;
                    case 192:
                        snd.play(R2);
                        rButton.setId(193);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        rButton.startAnimation(wobble3);
                        rButton.setImageResource(R.drawable.rice);
                        break;
                    case 193:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        rButton.startAnimation(fadein);
                        snd.play(R3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        rButton.setId(191);
                        rButton.setImageResource(R.drawable.r);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton sButton = (ImageButton) findViewById(R.id.sButton);
        sButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.sButton:
                        snd.play(S1);
                        sButton.setId(202);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        sButton.startAnimation(wobble);
                        break;
                    case 201:
                        snd.play(S1);
                        sButton.setId(202);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        sButton.startAnimation(wobble2);
                        break;
                    case 202:
                        snd.play(S2);
                        sButton.setId(203);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        sButton.startAnimation(wobble3);
                        sButton.setImageResource(R.drawable.strawberry);
                        break;
                    case 203:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        sButton.startAnimation(fadein);
                        snd.play(S3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        sButton.setId(201);
                        sButton.setImageResource(R.drawable.s);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton tButton = (ImageButton) findViewById(R.id.tButton);
        tButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.tButton:
                        snd.play(T1);
                        tButton.setId(212);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        tButton.startAnimation(wobble);
                        break;
                    case 211:
                        snd.play(T1);
                        tButton.setId(212);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        tButton.startAnimation(wobble2);
                        break;
                    case 212:
                        snd.play(T2);
                        tButton.setId(213);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        tButton.startAnimation(wobble3);
                        tButton.setImageResource(R.drawable.toast);
                        break;
                    case 213:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        tButton.startAnimation(fadein);
                        snd.play(T3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        tButton.setId(211);
                        tButton.setImageResource(R.drawable.t);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton uButton = (ImageButton) findViewById(R.id.uButton);
        uButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.uButton:
                        snd.play(U1);
                        uButton.setId(222);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        uButton.startAnimation(wobble);
                        break;
                    case 221:
                        snd.play(U1);
                        uButton.setId(222);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        uButton.startAnimation(wobble2);
                        break;
                    case 222:
                        snd.play(U2);
                        uButton.setId(223);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        uButton.startAnimation(wobble3);
                        uButton.setImageResource(R.drawable.urchin);
                        break;
                    case 223:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        uButton.startAnimation(fadein);
                        snd.play(U3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        uButton.setId(221);
                        uButton.setImageResource(R.drawable.u);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton vButton = (ImageButton) findViewById(R.id.vButton);
        vButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.vButton:
                        snd.play(V1);
                        vButton.setId(232);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        vButton.startAnimation(wobble);
                        break;
                    case 231:
                        snd.play(V1);
                        vButton.setId(232);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        vButton.startAnimation(wobble2);
                        break;
                    case 232:
                        snd.play(V2);
                        vButton.setId(233);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        vButton.startAnimation(wobble3);
                        vButton.setImageResource(R.drawable.vegetables);
                        break;
                    case 233:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        vButton.startAnimation(fadein);
                        snd.play(V3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        vButton.setId(231);
                        vButton.setImageResource(R.drawable.v);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton wButton = (ImageButton) findViewById(R.id.wButton);
        wButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.wButton:
                        snd.play(W1);
                        wButton.setId(242);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        wButton.startAnimation(wobble);
                        break;
                    case 241:
                        snd.play(W1);
                        wButton.setId(242);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        wButton.startAnimation(wobble2);
                        break;
                    case 242:
                        snd.play(W2);
                        wButton.setId(243);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        wButton.startAnimation(wobble3);
                        wButton.setImageResource(R.drawable.watermelon);
                        break;
                    case 243:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        wButton.startAnimation(fadein);
                        snd.play(W3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1500); }catch(InterruptedException e){ }
                        wButton.setId(241);
                        wButton.setImageResource(R.drawable.w);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton xButton = (ImageButton) findViewById(R.id.xButton);
        xButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.xButton:
                        snd.play(X1);
                        xButton.setId(252);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        xButton.startAnimation(wobble);
                        break;
                    case 251:
                        snd.play(X1);
                        xButton.setId(252);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        xButton.startAnimation(wobble2);
                        break;
                    case 252:
                        snd.play(X2);
                        xButton.setId(253);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        xButton.startAnimation(wobble3);
                        xButton.setImageResource(R.drawable.xshapedcake);
                        break;
                    case 253:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        xButton.startAnimation(fadein);
                        snd.play(X3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(2000); }catch(InterruptedException e){ }
                        xButton.setId(251);
                        xButton.setImageResource(R.drawable.x);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton yButton = (ImageButton) findViewById(R.id.yButton);
        yButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.yButton:
                        snd.play(Y1);
                        yButton.setId(262);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        yButton.startAnimation(wobble);
                        break;
                    case 261:
                        snd.play(Y1);
                        yButton.setId(262);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        yButton.startAnimation(wobble2);
                        break;
                    case 262:
                        snd.play(Y2);
                        yButton.setId(263);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        yButton.startAnimation(wobble3);
                        yButton.setImageResource(R.drawable.yogurt);
                        break;
                    case 263:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        yButton.startAnimation(fadein);
                        snd.play(Y3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        yButton.setId(261);
                        yButton.setImageResource(R.drawable.y);
                        break;

                    default:
                        break;

                }


            }


        });
        final ImageButton zButton = (ImageButton) findViewById(R.id.zButton);
        zButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.zButton:
                        snd.play(Z1);
                        zButton.setId(272);
                        Animation wobble = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        zButton.startAnimation(wobble);
                        break;
                    case 271:
                        snd.play(Z1);
                        zButton.setId(272);
                        Animation wobble2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        zButton.startAnimation(wobble2);
                        break;
                    case 272:
                        snd.play(Z2);
                        zButton.setId(273);
                        Animation wobble3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.wobble);
                        zButton.startAnimation(wobble3);
                        zButton.setImageResource(R.drawable.zucchini);
                        break;
                    case 273:
                        Animation fadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
                        zButton.startAnimation(fadein);
                        snd.play(Z3);
                        //Adjust sleep here based on how long the mp3 is for that word
                        try{ Thread.sleep(1000); }catch(InterruptedException e){ }
                        zButton.setId(271);
                        zButton.setImageResource(R.drawable.z);
                        break;

                    default:
                        break;

                }


            }


        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
//    public static class PlaceholderFragment extends Fragment {

//        public PlaceholderFragment() {
//        }

//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//            return rootView;
//        }
  //  }


}



